package by.danilau.routes_service;

import by.danilau.routes_service.domain.model.api.Connection;
import by.danilau.routes_service.domain.model.api.Location;
import by.danilau.routes_service.domain.model.dijkstra.Route;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = RoutesService.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AutoConfigureWebTestClient
public class RoutesServiceTests {

    @Autowired
    private WebTestClient client;

    @Test
    public void test_1() {
        String graph = "" +
                "A:B,1|C,7|D,5\n" +
                "B:C,1\n" +
                "C:D,1\n";

        String testCases = "" +
                "A,D,3\n" +
                "A,C,2\n";

        parametrizedTest(graph, testCases);
    }

    public void parametrizedTest(String graph, String testCases) {
        Map<String, Location> nodes = new HashMap<>();

        String[] nodeInfos = graph.split("\n");
        for (String nodeInfo : nodeInfos) {
            String[] nodeAndConnections = nodeInfo.split(":");
            String node = nodeAndConnections[0];
            String[] connections = nodeAndConnections[1].split("\\|");
            Location start = nodes.computeIfAbsent(node, this::createNode);
            for (String connection : connections) {
                String[] connectionData = connection.split(",");
                Location end = nodes.computeIfAbsent(connectionData[0], this::createNode);
                Long weight = Long.valueOf(Integer.valueOf(connectionData[1]));
                createConnection(start, end, weight);
            }
        }

        String[] tests = testCases.split("\n");
        for (String test : tests) {
            String[] testData = test.split(",");
            Location start = nodes.get(testData[0]);
            Location end = nodes.get(testData[1]);
            Long expectedLength = Long.valueOf(Integer.valueOf(testData[2]));
            testPath(start.getId(), end.getId(), expectedLength);
        }
    }

    private Location createNode(String name) {
        Location location = new Location();
        location.setName(name);
        Mono<Location> locationMono = Mono.just(location);
        return client.post().uri("/locations")
                .body(locationMono, Location.class)
                .exchange()
                .expectBody(Location.class)
                .returnResult()
                .getResponseBody();
    }

    private Connection createConnection(Location start, Location end, Long weight) {
        Connection connection = new Connection();
        connection.setStart(start);
        connection.setEnd(end);
        connection.setWeight(weight);
        Mono<Connection> connectionMono = Mono.just(connection);
        return client.post().uri("/connections")
                .body(connectionMono, Connection.class)
                .exchange()
                .expectBody(Connection.class)
                .returnResult()
                .getResponseBody();
    }

    private void testPath(Long startId, Long endId, Long expectedWeight) {
        String path = String.format("/routes/%s/%s", startId, endId);

        client.get().uri(path)
                .exchange()
                .expectStatus().isOk()
                .expectBody(Route.class)
                .value(Route::getLength, Matchers.equalTo(expectedWeight));
    }

}
