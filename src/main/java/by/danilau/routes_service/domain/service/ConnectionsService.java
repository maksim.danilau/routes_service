package by.danilau.routes_service.domain.service;

import by.danilau.routes_service.domain.model.api.Connection;
import by.danilau.routes_service.domain.model.api.Location;
import by.danilau.routes_service.domain.mapper.Mapper;
import by.danilau.routes_service.domain.model.entity.ConnectionEntity;
import by.danilau.routes_service.domain.model.entity.repository.ConnectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ConnectionsService {

    @Autowired
    private ConnectionRepository repository;

    @Autowired
    private Mapper mapper;

    public Connection createConnection(Connection connection) {
        ConnectionEntity entity = mapper.map(connection, ConnectionEntity.class);
        entity = repository.save(entity);
        return mapper.map(entity, Connection.class);
    }

    public Connection getConnection(Long id) {
        ConnectionEntity entity = repository.getOne(id);
        return mapper.map(entity, Connection.class);
    }

    public List<Connection> getConnections() {
        return repository.findAll()
                .stream()
                .map(entity -> mapper.map(entity, Connection.class))
                .collect(Collectors.toList());
    }

    public Connection updateConnection(Connection connection) {
        return Optional.of(connection)
                .map(Connection::getId)
                .map(repository::getOne)
                .map(entity -> mapper.map(connection, entity))
                .map(repository::save)
                .map(entity -> mapper.map(entity, Connection.class))
                .get();
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }

    public List<Connection> getConnectionsByLocation(Location location) {
        List<ConnectionEntity> entities = repository.findAllByStartIdOrEndId(location.getId());
        return entities.stream()
                .map(entity -> mapper.map(entity, Connection.class))
                .collect(Collectors.toList());
    }
}
