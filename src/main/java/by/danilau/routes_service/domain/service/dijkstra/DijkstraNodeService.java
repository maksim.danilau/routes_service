package by.danilau.routes_service.domain.service.dijkstra;

import by.danilau.routes_service.domain.model.dijkstra.DijkstraNode;

public interface DijkstraNodeService<T, D extends DijkstraNode<T>> {

    public D create(T value);

    public void enrichWithNeighbours(D node);
}
