package by.danilau.routes_service.domain.service.dijkstra;

import by.danilau.routes_service.domain.model.api.Location;
import by.danilau.routes_service.domain.model.dijkstra.DijkstraLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DijkstraLocationRouteService extends RouteService<Location, DijkstraLocation> {

    @Autowired
    public DijkstraLocationRouteService(DijkstraLocationNodeService builder) {
        super(builder);
    }
}
