package by.danilau.routes_service.domain.service.dijkstra;

import by.danilau.routes_service.domain.model.dijkstra.DijkstraNode;
import by.danilau.routes_service.domain.model.dijkstra.Route;

import java.util.*;


public abstract class RouteService<T, N extends DijkstraNode<T>> {

    private static final String UNREACHABLE_MESSAGE = "%s unreachable from %s";
    private DijkstraNodeService<T, N> dijkstraNodeService;

    public RouteService(DijkstraNodeService<T, N> dijkstraNodeService) {
        this.dijkstraNodeService = dijkstraNodeService;
    }

    public Route<T> findPath(T start, T destination) {
        Set<T> visited = new HashSet<>();
        Map<T, N> cache = new HashMap<>();
        T currentValue = start;
        while (!visited.contains(destination) && currentValue != null) {
            visited.add(currentValue);

            N currentNode = cache.computeIfAbsent(currentValue, dijkstraNodeService::create);
            if (currentValue.equals(start)) {
                currentNode.setDistanceFromStart(0L);
            }

            if (!currentNode.isNeighboursInitialized()) {
                dijkstraNodeService.enrichWithNeighbours(currentNode);
            }
            List<T> neighbours = currentNode.getNeighbours();

            N closestNode = null;
            for (T neighbour : neighbours) {
                N neighbourNode = cache.computeIfAbsent(neighbour, dijkstraNodeService::create);

                Long distanceFromCurrent = currentNode.getDistanceFromStart() + currentNode.getDistanceTo(neighbour);
                if (neighbourNode.getDistanceFromStart() > distanceFromCurrent) {
                    neighbourNode.setDistanceFromStart(distanceFromCurrent);
                    neighbourNode.setPrev(currentNode);
                }

                if ((closestNode == null || closestNode.getDistanceFromStart() > neighbourNode.getDistanceFromStart())
                        && !visited.contains(neighbour)) {
                    closestNode = neighbourNode;
                }
            }
            currentValue = Optional.ofNullable(closestNode).map(DijkstraNode::getValue).orElse(null);
        }
        if (visited.contains(destination)) {
            return buildTheResult(destination, cache);
        } else {
            return buildUnreachableResult(start, destination);
        }
    }

    private Route<T> buildTheResult(T destination, Map<T, N> visited) {
        Route<T> result = new Route<>();
        DijkstraNode<T> destinationNode = visited.get(destination);
        result.setLength(destinationNode.getDistanceFromStart());
        LinkedList<T> descendingRoute = new LinkedList<>();
        while (destinationNode != null) {
            descendingRoute.add(destinationNode.getValue());
            destinationNode = destinationNode.getPrev();
        }
        List<T> path = new LinkedList<>();
        descendingRoute.descendingIterator().forEachRemaining(path::add);
        result.setRoute(path);
        return result;
    }

    private Route<T> buildUnreachableResult(T start, T destination) {
        Route<T> res = new Route<>();
        res.setMessage(String.format(UNREACHABLE_MESSAGE, destination, start));
        return res;
    }
}
