package by.danilau.routes_service.domain.service;

import by.danilau.routes_service.domain.model.api.Location;
import by.danilau.routes_service.domain.mapper.Mapper;
import by.danilau.routes_service.domain.model.entity.LocationEntity;
import by.danilau.routes_service.domain.model.entity.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LocationsService {

    @Autowired
    private Mapper mapper;
    @Autowired
    private LocationRepository repository;

    public Location getLocation(Long id) {
        LocationEntity entity = repository.getOne(id);
        return mapper.map(entity, Location.class);
    }

    public Location createLocation(Location location) {
        LocationEntity entity = mapper.map(location, LocationEntity.class);
        entity = repository.save(entity);
        return mapper.map(entity, Location.class);
    }

    public List<Location> getLocations() {
        return repository.findAll()
                .stream()
                .map(entity -> mapper.map(entity, Location.class))
                .collect(Collectors.toList());
    }

    public Location updateLocation(Location location) {
        LocationEntity entity = repository.getOne(location.getId());
        entity = mapper.map(location, entity);
        entity = repository.save(entity);
        return mapper.map(entity, location);
    }

    public void deleteLocation(Long id) {
        repository.deleteById(id);
    }
}
