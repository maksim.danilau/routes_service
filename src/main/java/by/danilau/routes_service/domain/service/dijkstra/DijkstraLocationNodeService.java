package by.danilau.routes_service.domain.service.dijkstra;

import by.danilau.routes_service.domain.model.api.Connection;
import by.danilau.routes_service.domain.model.api.Location;
import by.danilau.routes_service.domain.model.dijkstra.DijkstraLocation;
import by.danilau.routes_service.domain.service.ConnectionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DijkstraLocationNodeService implements DijkstraNodeService<Location, DijkstraLocation> {

    @Autowired
    private ConnectionsService connectionService;

    public DijkstraLocation create(Location location) {
        return new DijkstraLocation(location);
    }

    @Override
    public void enrichWithNeighbours(DijkstraLocation node) {
        Location location = node.getValue();
        Map<Location, Long> neighboursMap = new HashMap<>();
        List<Connection> connections = connectionService.getConnectionsByLocation(location);
        connections.forEach(connection -> {
            Location start = connection.getStart();
            Location end = connection.getEnd();
            Long weight = connection.getWeight();
            Location key;
            if (start.getId().equals(location.getId())) {
                key = end;
            } else {
                key = start;
            }
            neighboursMap.put(key, weight);
        });
        node.setNeighborsMap(neighboursMap);
    }
}
