package by.danilau.routes_service.domain.model.entity.repository;

import by.danilau.routes_service.domain.model.entity.ConnectionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConnectionRepository extends JpaRepository<ConnectionEntity, Long> {

    @Query("select ce from ConnectionEntity ce where ce.end.id = :id or ce.start.id = :id")
    List<ConnectionEntity> findAllByStartIdOrEndId(@Param("id") Long id);
}
