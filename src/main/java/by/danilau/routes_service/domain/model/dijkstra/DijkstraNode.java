package by.danilau.routes_service.domain.model.dijkstra;

import lombok.Data;
import lombok.NonNull;

import java.util.List;

@Data
public abstract class DijkstraNode<T> {

    @NonNull
    private T value;
    private DijkstraNode<T> prev;
    private Long distanceFromStart = Long.MAX_VALUE;

    public abstract List<T> getNeighbours();

    public abstract Long getDistanceTo(T value);

    public abstract boolean isNeighboursInitialized();
}
