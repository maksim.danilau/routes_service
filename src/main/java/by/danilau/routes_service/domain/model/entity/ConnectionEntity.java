package by.danilau.routes_service.domain.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
public class ConnectionEntity {

    @Id
    @GeneratedValue
    @Getter
    @Setter
    private Long id;

    @ManyToOne
    @Getter
    @Setter
    private LocationEntity start;

    @ManyToOne
    @Getter
    @Setter
    private LocationEntity end;

    @Getter
    @Setter
    private Long weight;
}
