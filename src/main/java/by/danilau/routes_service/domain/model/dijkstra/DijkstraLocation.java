package by.danilau.routes_service.domain.model.dijkstra;

import by.danilau.routes_service.domain.model.api.Location;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DijkstraLocation extends DijkstraNode<Location> {

    @Getter
    @Setter
    private Map<Location, Long> neighborsMap;

    public DijkstraLocation(Location location) {
        super(location);
    }

    @Override
    public List<Location> getNeighbours() {
        return new ArrayList<>(neighborsMap.keySet());
    }

    @Override
    public Long getDistanceTo(Location value) {
        return neighborsMap.get(value);
    }

    @Override
    public boolean isNeighboursInitialized() {
        return neighborsMap != null;
    }
}
