package by.danilau.routes_service.domain.model.api;

import lombok.Data;

import java.io.Serializable;

@Data
public class Location implements Serializable {

    private Long id;
    private String name;
}
