package by.danilau.routes_service.domain.model.dijkstra;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Data
@JsonInclude(NON_NULL)
public class Route<T> {

    private List<T> route;
    private Long length;
    private String message;
}
