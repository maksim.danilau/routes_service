package by.danilau.routes_service.domain.model.api;

import lombok.Data;

import java.io.Serializable;

@Data
public class Connection implements Serializable {

    private Long id;
    private Location start;
    private Location end;
    private Long weight;
}
