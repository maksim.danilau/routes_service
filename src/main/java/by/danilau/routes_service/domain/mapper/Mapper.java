package by.danilau.routes_service.domain.mapper;

import by.danilau.routes_service.domain.mapper.converter.BaseConverter;
import lombok.Data;
import lombok.NonNull;
import org.hibernate.proxy.HibernateProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Service
public class Mapper {

    @Autowired
    private Set<BaseConverter> converters;
    private Map<ClassPair, BaseConverter> convertersMap;

    @PostConstruct
    private void init() {
        convertersMap = new HashMap<>();
        converters.forEach(converter -> {
            ClassPair key = new ClassPair(converter.getSourceClass(), converter.getDestinationClass());
            convertersMap.put(key, converter);
        });
    }

    public <S, D> D map(S source, Class<D> destinationClass) {
        BaseConverter<S, D> converter = convertersMap.get(new ClassPair(extractClass(source), destinationClass));
        D destination;
        try {
            destination = destinationClass.getConstructor().newInstance();
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException ex) {
            throw new MapperError(ex);
        }
        converter.convert(source, destination);
        return destination;
    }

    public <S, D> D map(S source, D destination) {
        BaseConverter<S, D> converter = convertersMap.get(new ClassPair(extractClass(source), extractClass(destination)));
        converter.convert(source, destination);
        return destination;
    }

    private <T> Class extractClass(T value) {
        if (value instanceof HibernateProxy) {
            return ((HibernateProxy) value).getHibernateLazyInitializer().getPersistentClass();
        }
        return value.getClass();
    }

    @Data
    private static class ClassPair {

        @NonNull
        private Class source;
        @NonNull
        private Class destination;
    }
}
