package by.danilau.routes_service.domain.mapper.converter;

import by.danilau.routes_service.domain.model.api.Location;
import by.danilau.routes_service.domain.model.entity.LocationEntity;
import org.springframework.stereotype.Component;

@Component
public class LocationApiToLocationJpaConverter implements BaseConverter<Location, LocationEntity> {

    @Override
    public void convert(Location source, LocationEntity destination) {
        destination.setId(source.getId());
        destination.setName(source.getName());
    }
}
