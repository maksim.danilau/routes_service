package by.danilau.routes_service.domain.mapper.converter;

import by.danilau.routes_service.domain.model.api.Connection;
import by.danilau.routes_service.domain.mapper.Mapper;
import by.danilau.routes_service.domain.model.entity.ConnectionEntity;
import by.danilau.routes_service.domain.model.entity.LocationEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConnectionApiToConnectionEntityConverter implements BaseConverter<Connection, ConnectionEntity> {

    @Autowired
    private Mapper mapper;

    @Override
    public void convert(Connection source, ConnectionEntity destination) {
        destination.setId(source.getId());
        destination.setStart(mapper.map(source.getStart(), LocationEntity.class));
        destination.setEnd(mapper.map(source.getEnd(), LocationEntity.class));
        destination.setWeight(source.getWeight());
    }
}
