package by.danilau.routes_service.domain.mapper;

public class MapperError extends Error {

    public MapperError(Throwable caused) {
        super("Destination class should have public constructor without arguments", caused);
    }
}
