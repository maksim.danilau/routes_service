package by.danilau.routes_service.domain.mapper.converter;

import java.lang.reflect.ParameterizedType;

public interface BaseConverter<S, D> {

    public void convert(S source, D destination);

    default Class<S> getSourceClass() {
        return (Class<S>) ((ParameterizedType) getClass().getGenericInterfaces()[0]).getActualTypeArguments()[0];
    }

    default Class<D> getDestinationClass() {
        return (Class<D>) ((ParameterizedType) getClass().getGenericInterfaces()[0]).getActualTypeArguments()[1];
    }
}
