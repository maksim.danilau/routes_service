package by.danilau.routes_service.domain.mapper.converter;

import by.danilau.routes_service.domain.model.api.Location;
import by.danilau.routes_service.domain.model.entity.LocationEntity;
import org.springframework.stereotype.Component;

@Component
public class LocationJpaToLocationApiConverter implements BaseConverter<LocationEntity, Location> {

    @Override
    public void convert(LocationEntity source, Location destination) {
        destination.setId(source.getId());
        destination.setName(source.getName());
    }
}
