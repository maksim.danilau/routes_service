package by.danilau.routes_service.domain.mapper.converter;

import by.danilau.routes_service.domain.model.api.Connection;
import by.danilau.routes_service.domain.model.api.Location;
import by.danilau.routes_service.domain.mapper.Mapper;
import by.danilau.routes_service.domain.model.entity.ConnectionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConnectionEntityToConnectionApiConverter implements BaseConverter<ConnectionEntity, Connection> {

    @Autowired
    private Mapper mapper;

    @Override
    public void convert(ConnectionEntity source, Connection destination) {
        destination.setId(source.getId());
        destination.setStart(mapper.map(source.getStart(), Location.class));
        destination.setEnd(mapper.map(source.getEnd(), Location.class));
        destination.setWeight(source.getWeight());
    }
}
