package by.danilau.routes_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RoutesService {

    public static void main(String[] args) {
        SpringApplication.run(RoutesService.class, args);
    }
}
