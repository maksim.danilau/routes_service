package by.danilau.routes_service.web;

import by.danilau.routes_service.domain.model.api.Location;
import by.danilau.routes_service.domain.service.LocationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("locations")
public class LocationController {

    @Autowired
    private LocationsService service;

    @PostMapping
    public Location createLocation(@RequestBody Location location) {
        return service.createLocation(location);
    }

    @GetMapping("/{id}")
    public Location getLocation(@PathVariable Long id) {
        return service.getLocation(id);
    }

    @GetMapping
    public List<Location> getLocations() {
        return service.getLocations();
    }

    @PutMapping("/{id}")
    public Location updateLocation(@PathVariable Long id, @RequestBody Location location) {
        location.setId(id);
        return service.updateLocation(location);
    }

    @DeleteMapping("/{id}")
    public void deleteLocation(@PathVariable Long id) {
        service.deleteLocation(id);
    }
}
