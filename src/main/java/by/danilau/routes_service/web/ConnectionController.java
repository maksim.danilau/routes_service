package by.danilau.routes_service.web;

import by.danilau.routes_service.domain.model.api.Connection;
import by.danilau.routes_service.domain.service.ConnectionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("connections")
public class ConnectionController {

    @Autowired
    private ConnectionsService service;

    @PostMapping
    public Connection createConnection(@RequestBody Connection connection) {
        return service.createConnection(connection);
    }

    @GetMapping("/{id}")
    public Connection getConnection(@PathVariable Long id) {
        return service.getConnection(id);
    }

    @GetMapping
    public List<Connection> getConnections() {
        return service.getConnections();
    }

    @PutMapping("/{id}")
    public Connection updateConnection(@PathVariable Long id, @RequestBody Connection connection) {
        connection.setId(id);
        return service.updateConnection(connection);
    }

    @DeleteMapping("/{id}")
    public void deleteConnection(@PathVariable Long id) {
        service.delete(id);
    }
}
