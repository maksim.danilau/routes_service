package by.danilau.routes_service.web;

import by.danilau.routes_service.domain.model.api.Location;
import by.danilau.routes_service.domain.model.dijkstra.Route;
import by.danilau.routes_service.domain.service.LocationsService;
import by.danilau.routes_service.domain.service.dijkstra.DijkstraLocationRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("routes")
public class RouteController {

    @Autowired
    private LocationsService locationsService;

    @Autowired
    private DijkstraLocationRouteService routeService;

    @GetMapping("/{start}/{end}")
    public Route<Location> findRoute(@PathVariable Long start, @PathVariable Long end) {
        Location startLocation = locationsService.getLocation(start);
        Location endLocation = locationsService.getLocation(end);
        return routeService.findPath(startLocation, endLocation);
    }
}
