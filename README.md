# The City distance Project

## About

This micro service calculates distance between two cities and all possible paths.

## Technical Stack

- Java 1.8+
- Maven 3.5+
- Spring boot 2.1.0.RELEASE+
- Lombok project
- JPA with H2 for explanation
- Swagger 2 API documentation
- REST API model validation 

## Running application
Just execute **run.sh** script or execute **mvn spring-boot:run** in terminal 
## Testing application

For more convenience here listed curls which can be used for testing application:

``` jshelllanguage
curl 'http://localhost:8080/cities/NY/Washington' -H 'Accept-Encoding: gzip, deflate, br' -H 'Accept-Language: en-US,en;q=0.9,ru;q=0.8' -H 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJEZW1vIiwiaWF0IjoxNTQ1OTA5MTk1LCJleHAiOjE1NDU5MTI3OTV9.nx7MLslbQzlWjZq7PzGn9q1M-NRLap4S74KyxRkPpWsqSpJVwitK_Lo4hFiSAvBjqxxq9tr3rbRJQV3RCSlD-w' -H 'Content-Type: application/json' -H 'Accept: */*' -H 'Cache-Control: no-cache' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36' -H 'Connection: keep-alive' --compressed
```
